package main

import "flag"
import "fmt"
import "io/ioutil"
import "os"
import "strings"

func main() {
	recursivePtr := flag.Bool("r", false, "Recursively clean a directory")
	undoPtr := flag.Bool("u", false, "Undo cleaning")
	pathPtr := flag.String("target", "", "Target file or directory to clean")

	flag.Parse()

	if *pathPtr == "" {
		fmt.Println("Please select a target")
		os.Exit(1)
	}

	err := handleTarget(*pathPtr, *recursivePtr, *undoPtr)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func handleTarget(targetPath string, recursive bool, undo bool) error {
	uri, err := os.Stat(targetPath)
	if err != nil {
		return err
	}
	switch mode := uri.Mode(); {
	case mode.IsDir():
		files, err := ioutil.ReadDir(targetPath)
		if err != nil {
			return err
		}
		for _, f := range files {
			err := handleFile(fmt.Sprintf("%s/%s", targetPath, f.Name()), undo)
			if err != nil {
				return err
			}
		}
	case mode.IsRegular():
		handleFile(targetPath, undo)
	}
	return nil
}

func handleFile(pathToFile string, undo bool) error {
	f, err := ioutil.ReadFile(pathToFile)
	if err != nil {
		return err
	}
	var newContents string
	if undo {
		newContents = strings.ReplaceAll(string(f), ";", ";")
	} else {
		newContents = strings.ReplaceAll(string(f), ";", ";")
	}
	err = ioutil.WriteFile(pathToFile, []byte(newContents), 0)
	return err
}
